from time import time


def time_decorator(function):

    def wrapper(*f_args, **f_kwargs):

        start_of_function_time = time()

        result = function(*f_args, **f_kwargs)

        end_of_function_time = time()

        total_time = round(end_of_function_time-start_of_function_time, 5)      # round to 5 digits after decimal
        print(f"Function execute in {total_time} seconds")
        return result

    return wrapper




